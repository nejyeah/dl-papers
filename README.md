﻿# Paper List for Efficient

标签（空格分隔）： paper

---
During my work as a computer vision engineer in DL, I have read many papers which are either related to my work or interest me. To record what I have read, I decide to maintain the paper reading list. To share and discuss with people who have the same interest, I will post blogs of the papers that I have read. For now, my major attention is focused on "Model compression and Acceleration" and "Face Detection and Recognition". I have done some projects in "GAN"(like demesh in face images) and "OCR"(like recognition in the wild, recognition of Idcard), and I will continue to follow the lastest reasearch in these domain. Please feel free to discuss with me in the Issue page or in my [blog website](http://nejyeah.github.io/). 

Papers in bold are those that I actually used in my work or I highly recommend to read.

---
[TOC]

---
## 1. Model Compression and Acceleration
### 1.1 Quantization
- Adaptive Quantization of Neural Networks, ICLR2018, [paper](https://arxiv.org/abs/1712.01048), [code](), [blog]()
- Alternating Multi-Bit Quantization for Recurrent Neural Networks, ICLR2018, [paper](https://arxiv.org/abs/1802.00150), [code](), [blog]()
- Loss-Aware Weight Quantization of Deep Networks, ICLR2018, [paper](https://arxiv.org/abs/1802.08635), [code](), [blog]()
- Model Compression via Distillation and Quantization, ICLR2018, [paper](https://arxiv.org/abs/1802.05668), [code](), [blog]()
- Variational Network Quantization, ICLR2018, [paper](https://openreview.net/pdf?id=ry-TW-WAb), [code](), [blog]()
- Quantization and Training of Neural Networks for Efficient Integer-Arithmetic-Only Inference, arXiv2017Dec, [paper](https://arxiv.org/abs/1712.05877), [code](), [blog]()
- Ternary Neural Networks with Fine-Grained Quantization, arXiv2017May, [paper](https://arxiv.org/abs/1705.01462), [code](), [blog]()
- Towards Accurate Binary Convolutional Neural Network, NIPS2017, [paper](https://arxiv.org/abs/1711.11294), [code](), [blog]()
- Performance Guaranteed Network Acceleration via High-Order Residual Quantization, ICCV2017, [paper](https://arxiv.org/abs/1708.08687), [code](), [blog]()
- Incremental Network Quantization: Towards Lossless CNNs with Low-Precision Weights, ICLR2017, [paper](https://arxiv.org/abs/1702.03044), [code](), [blog]()
- Efficient Methods and Hardware for Deep Learning, phD thesis2017 of HanSong,  [paper](https://purl.stanford.edu/qf934gh3708), [code](), [blog]()
- Dynamic Network Surgery for Efficient DNNs, arXiv2016Nov, [paper](https://arxiv.org/abs/1608.04493), [code](), [blog]()
- **XNOR-Net: ImageNet Classification Using Binary Convolutional Neural Networks**, arXiv2016Aug, [paper](https://arxiv.org/abs/1603.05279), [code](), [blog]()
- Binarized Neural Networks: Training Neural Networks withWeights and Activations Constrained to +1 or -1, arXiv2016Feb, [paper](https://arxiv.org/abs/1602.02830), [code](https://github.com/itayhubara/BinaryNet), [blog]()
- EIE: Efficient Inference Engine on Compressed Deep Neural Network, ISCA2016, [paper](https://arxiv.org/abs/1602.01528), [code](), [blog]()
- Deep Compression: Compressing Deep Neural Networks with Pruning, Trained Quantization and Huffman Coding, ICLR2016, [paper](https://arxiv.org/abs/1510.00149), [code](), [blog]()
- Hardware-oriented Approximation of Convolutional Neural Networks, ICLR2016, [paper](https://arxiv.org/pdf/1604.03168.pdf), [code](), [blog]()

### 1.2 Sparsity regularizers & Pruning
- Efficient Sparse-Winograd Convolutional Neural Networks, ICLR2018, [paper](https://arxiv.org/abs/1802.06367), [code](), [blog]()
- Learning Efficient Convolutional Networks through Network Slimming, ICCV2017, [paper](https://arxiv.org/abs/1708.06519), [code](), [blog]()
- Pruning Convolutional Neural Networks for Resource Efficient Inference, ICLR2017,  [paper](https://arxiv.org/abs/1611.06440), [code](), [blog]()
- Fast Algorithms for Convolutional Neural Networks, arXiv2015Nov, [paper](https://arxiv.org/abs/1509.09308), [code](), [blog]()


### 1.3 Network Architecture
- Multi-Scale Dense Networks for Resource Efficient Image Classification, ICLR2018, [paper](https://arxiv.org/abs/1703.09844), [code](), [blog]()
- Inverted Residuals and Linear Bottlenecks: Mobile Networks for Classification, Detection and Segmentation， arXiv2018Jan, [paper](https://arxiv.org/abs/1801.04381), [code](), [blog]()
- ShuffleNet: An Extremely Efficient Convolutional Neural Network for Mobile Devices, arXiv2017Jul, [paper](https://arxiv.org/abs/1707.01083), [code](), [blog]()
- MobileNets: Efficient Convolutional Neural Networks for Mobile Vision Applications, arXiv2017Apr, [paper](https://arxiv.org/abs/1704.04861), [code](), [blog]()
- Xception: Deep Learning with Depthwise Separable Convolutions, arXiv2017Apr, [paper](https://arxiv.org/abs/1610.02357), [code](), [blog]()
- Aggregated Residual Transformations for Deep Neural Networks, arXiv2017Apr, [paper](https://arxiv.org/abs/1611.05431), [code](), [blog]()

### 1.4 Distilling
- Paying More Attention to Attention: Improving the Performance of Convolutional Neural Networks via Attention Transfer, ICLR2017, [paper](https://arxiv.org/abs/1612.03928), [code](), [blog]()
- Distilling the Knowledge in a Neural Network, arXiv2015Mar, [paper](https://arxiv.org/abs/1503.02531), [code](), [blog]()

### 1.5 Hardware Acceleration
- ESPRESSO: Efficient Forward Propagation for Binary Deep Neural Networks, ICLR2018, [paper](https://arxiv.org/abs/1705.07175), [code](), [blog]()
- Eyeriss: An Energy-Efficient Reconfigurable Accelerator for Deep Convolutional
Neural Networks, JSSC2016, [paper](http://www.rle.mit.edu/eems/wp-content/uploads/2016/11/eyeriss_jssc_2017.pdf), [code](), [blog]()
- Ristretto: Hardware-Oriented Approximation of Convolutional Neural Networks, Master Thesis2016, [paper](https://arxiv.org/abs/1605.06402), [code](), [blog]()



## 2. Face Detection and Recognition
### 2.1 Face Recognition
- ArcFace: Additive Angular Margin Loss for Deep Face Recognition, arXiv2018Jan, [paper](https://arxiv.org/abs/1801.07698), [code](https://github.com/deepinsight/insightface), [blog]()
- Additive Margin Softmax for Face Verification, arXiv2018Jan, [paper](https://arxiv.org/abs/1801.05599), [code](), [blog]()
- Multi-Level Factorisation Net for Person Re-Identification, CVPR2018, [paper](https://arxiv.org/abs/1803.09132), [code](), [blog]()
- Efficient and Deep Person Re-Identification using Multi-Level Similarity, CVPR2018, [paper](https://arxiv.org/abs/1803.11353), [code](), [blog]()
- Pose-Robust Face Recognition via Deep Residual Equivariant Mapping, CVPR2018, [paper](https://arxiv.org/abs/1803.00839), [code](), [blog]()
- SphereFace: Deep Hypersphere Embedding for Face Recognition, arXiv2017Nov, [paper](https://arxiv.org/abs/1704.08063), [code](), [blog]()
- Rethinking Feature Discrimination and Polymerization for Large-scale Recognition, arXiv2017Oct, [paper](https://arxiv.org/abs/1710.00870), [code](), [blog]()
- NormFace: L2 Hypersphere Embedding for Face Verification, arXiv2017Jul, [paper](https://arxiv.org/abs/1704.06369), [code](), [blog]()
- Learning Deep Features via Congenerous Cosine Loss for Person Recognition, arXiv2017Mar, [paper](https://arxiv.org/abs/1702.06890), [code](), [blog]()
- How far are we from solving the 2D & 3D Face Alignment problem? (and a
dataset of 230,000 3D facial landmarks), ICCV2017, [paper](https://arxiv.org/abs/1703.07332), [code](), [blog]()
- Large-Margin Softmax Loss for Convolutional Neural Networks, arXiv2016Sep, [paper](https://arxiv.org/abs/1612.02295), [code](), [blog]()
- Latent Factor Guided Convolutional Neural Networks for Age-Invariant Face
Recognition, CVPR2016, [paper](https://pdfs.semanticscholar.org/4bd3/de97b256b96556d19a5db71dda519934fd53.pdf), [code](), [blog]()
- A Discriminative Feature Learning Approach for Deep Face Recognition, ECCV2016, [paper](https://ydwen.github.io/papers/WenECCV16.pdf), [code](), [blog]()
- FaceNet: A Unified Embedding for Face Recognition and Clustering, arXiv2015Jun, [paper](https://arxiv.org/abs/1503.03832), [code](), [blog](http://nejyeah.github.io/2018/02/11/Face%20Recognition%20by%20FaceNet/)
- Deep Learning Face Representation by Joint Identification-Verification, NIPS2014, [paper](https://arxiv.org/abs/1406.4773), [code](), [blog]()

### 2.2 Face Detection
- Beyond Trade-off: Accelerate FCN-based Face Detector with Higher Accuracy, CVPR2018, [paper](https://arxiv.org/abs/1804.05197), [code](), [blog]()
- S$^3$FD: Single Shot Scale-invariant Face Detector, arXiv2017Nov, [paper](https://arxiv.org/abs/1708.05237), [code](https://github.com/sfzhang15/SFD), [blog]()
- Face Attention Network: An Effective Face Detector for the Occluded Faces, arXiv2017Nov, [paper](https://arxiv.org/abs/1711.07246), [code](), [blog]()
- Detecting Faces Using Region-based Fully Convolutional Networks, arXiv2017Sep, [paper](https://arxiv.org/abs/1709.05256), [code](), [blog]()
- SSH: Single Stage Headless Face Detector, arXiv2017Oct, [paper](https://arxiv.org/abs/1708.03979), [code](https://github.com/mahyarnajibi/SSH), [blog](http://nejyeah.github.io/2018/02/11/Face%20Detection%20by%20SSH/)
- Joint Face Detection and Alignment using Multi-task Cascaded Convolutional Networks, 2016, [paper](https://arxiv.org/abs/1604.02878), [code](https://github.com/kpzhang93/MTCNN_face_detection_alignment), [blog](http://nejyeah.github.io/2018/02/11/Face%20Detection%20by%20MTCNN/)
- Funnel-Structured Cascade for Multi-View Face Detection with Alignment-Awareness, arXiv2016Sep, [paper](https://arxiv.org/abs/1609.07304), [code](https://github.com/seetaface/SeetaFaceEngine), [blog]()
- Multi-view Face Detection Using Deep Convolutional Neural Networks, 2015, [paper](https://arxiv.org/abs/1502.02766), [code](), [blog]()
- MegaFace: A Million Faces for Recognition at Scale, arXiv2015Sep, [paper](https://arxiv.org/abs/1505.02108), [code](), [blog]()

## 3. GAN
- Global and Local Consistent Age Generative Adversarial Networks, arXiv2018Jan, [paper](https://arxiv.org/abs/1801.08390), [code](), [blog]()
- Generating Adversarial Examples with Adversarial Networks, arXiv2018Jan, [paper](https://arxiv.org/abs/1801.02610), [code](), [blog]()
- AmbientGAN: Generative models from lossy measurements, ICLR2018_Oral, [paper](https://openreview.net/pdf/81dea91dc06b9988c2e2dbec7e07ed5e3942e5f2.pdf), [code](https://github.com/AshishBora/ambient-gan), [blog]()
- **Progressive Growing of GANs for Improved Quality, Stability, and Variation**, ICLR2018_Oral, [paper](https://arxiv.org/abs/1710.10196), [code](https://github.com/tkarras/progressive_growing_of_gans), [blog]()
- StackGAN++: Realistic Image Synthesis with Stacked Generative Adversarial Networks, arXiv2017Dec, [paper](https://arxiv.org/abs/1710.10916), [code](), [blog]()
- How Generative Adversarial Networks and Its Variants Work:An Overview of GAN, arXiv2017Nov, [paper](https://arxiv.org/abs/1711.05914), [code](), [blog]()
- Learning Face Age Progression: A Pyramid Architecture of GANs, arXiv2017Nov, [paper](https://arxiv.org/abs/1711.10352), [code](), [blog]()
- High-Resolution Image Synthesis and Semantic Manipulation with Conditional GANs, arXiv2017Nov, [paper](https://arxiv.org/abs/1711.11585), [code](https://github.com/NVIDIA/pix2pixHD), [blog]()
- Toward Multimodal Image-to-Image Translation, arXiv2017Nov, [paper](https://arxiv.org/abs/1711.11586), [code](https://github.com/junyanz/BicycleGAN), [blog]()
- **Unpaired Image-to-Image Translation using Cycle-Consistent Adversarial Networks**, arXiv2017Nov, [paper](https://arxiv.org/abs/1703.10593), [code](https://github.com/junyanz/CycleGAN), [blog]()
- DualGAN: Unsupervised Dual Learning for Image-to-Image Translation, arXiv2017Aug,  [paper](https://arxiv.org/abs/1704.02510), [code](), [blog]()
- Learning to Discover Cross-Domain Relations with Generative Adversarial Networks, arXiv2017May, [paper](https://arxiv.org/abs/1703.05192), [code](), [blog]()
- Dual-Agent GANs for Photorealistic and Identity Preserving Profile Face Synthesis, NIPS2017, [paper](https://papers.nips.cc/paper/6612-dual-agent-gans-for-photorealistic-and-identity-preserving-profile-face-synthesis), [code](), [blog]()
- **Image-to-Image Translation with Conditional Adversarial Networks**, CVPR2017, [paper](https://arxiv.org/abs/1611.07004), [code](https://github.com/phillipi/pix2pix), [blog]()
- Learning from Simulated and Unsupervised Images through Adversarial Training, CVPR2017, [paper](https://arxiv.org/abs/1612.07828), [code](), [blog]()
- Photographic Image Synthesis with Cascaded Refinement Networks, ICCV2017, [paper](https://arxiv.org/abs/1707.09405), [code](), [blog]()
- Beyond Face Rotation: Global and Local Perception GAN for Photorealistic and
Identity Preserving Frontal View Synthesis, ICCV2017, [paper](https://arxiv.org/abs/1704.04086), [code](), [blog]()
- PixelCNN++: Improving the PixelCNN with Discretized Logistic Mixture Likelihood and Other Modifications, ICLR2017, [paper](https://arxiv.org/abs/1701.05517), [code](), [blog]()
- Conditional Image Synthesis with Auxiliary Classifier GANs, ICLR2017, [paper](https://arxiv.org/abs/1610.09585), [code](), [blog]()
- Conditional Image Generation with PixelCNN Decoders, arXiv2016Jun, [paper](https://arxiv.org/abs/1606.05328), [code](), [blog]()
- Perceptual Losses for Real-Time Style Transfer and Super-Resolution, arXiv2016Mar, [paper](https://arxiv.org/abs/1603.08155), [code](), [blog]()
- InfoGAN: Interpretable Representation Learning by Information Maximizing Generative Adversarial Nets, arXiv2016Jun, [paper](https://arxiv.org/abs/1606.03657), [code](), [blog]()
- Unsupervised Representation Learning with Deep Convolutional Generative Adversarial Networks, ICLR2016, [paper](https://arxiv.org/abs/1511.06434), [code](), [blog]()
- NIPS 2016 Tutorial: Generative Adversarial Networks, NIPS2016, [paper](https://arxiv.org/abs/1701.00160), [code](), [blog]()
- Conditional Generative Adversarial Nets, arXiv2014, [paper](https://arxiv.org/abs/1411.1784), [code](), [blog]()
- Generative Adversarial Nets, 2014, [paper](https://arxiv.org/abs/1406.2661), [code](), [blog]()

## 4. OCR
### 4.1 Recognition
- FOTS: Fast Oriented Text Spotting with a Unified Network, arXiv2018Jan, [paper](https://arxiv.org/abs/1801.01671), [code](), [blog]()
- AON: Towards Arbitrarily-Oriented Text Recognition, CVPR2018, [paper](https://arxiv.org/abs/1711.04226), [code](), [blog]()
- SEE: Towards Semi-Supervised End-to-End Scene Text Recognition, arXiv2017Dec, [paper](https://arxiv.org/abs/1712.05404), [code](), [blog]()
- Arbitrarily-Oriented Text Recognition, arXiv2017Nov, [paper](https://arxiv.org/abs/1711.04226), [code](), [blog]()
- Focusing Attention: Towards Accurate Text Recognition in Natural Images, arXiv2017Sep, [paper](https://arxiv.org/abs/1709.02054), [code](), [blog]()
- Reading Scene Text with Attention Convolutional Sequence Modeling, arXiv2017Sep, [paper](https://arxiv.org/abs/1709.04303), [code](), [blog]() 
- Scene Text Recognition with Sliding Convolutional Character Models, arXiv2017Sep, [paper](https://arxiv.org/abs/1709.01727), [code](), [blog]()
- Towards End-to-end Text Spotting with Convolutional Recurrent Neural Networks, arXiv2017Jul, [paper](https://arxiv.org/abs/1707.03985), [code](), [blog]()
- STN-OCR: A single Neural Network for Text Detection and Text Recognition, arXiv2017Jul, [paper](https://arxiv.org/abs/1707.08831), [code](https://github.com/Bartzi/stn-ocr), [blog]()
- Visual attention models for scene text recognition, arXiv2017Jun, [paper](https://arxiv.org/abs/1706.01487), [code](), [blog]()
- **Attention-based Extraction of Structured Information from Street View Imagery**, arXiv2017May, [paper](https://arxiv.org/abs/1704.03549), [code](https://github.com/tensorflow/models/tree/master/research/attention_ocr), [blog]()
- End-to-End Interpretation of the French Street Name Signs Dataset, arXiv2017Feb, [paper](https://arxiv.org/abs/1702.03970), [code](), [blog]()
- Gated Recurrent Convolution Neural Network for OCR, NIPS2017, [paper](https://papers.nips.cc/paper/6637-gated-recurrent-convolution-neural-network-for-ocr.pdf), [code](), [blog]()
- Robust Scene Text Recognition with Automatic Rectification, arXiv2016Apr, [paper](https://arxiv.org/abs/1603.03915), [code](), [blog]()
- Recursive Recurrent Nets with Attention Modeling for OCR in the Wild, arXiv2016Mar, [paper](https://arxiv.org/abs/1603.03101), [code](), [blog]()
- Connectionist Temporal Classification: Labelling Unsegmented Sequence Data with Recurrent Neural Networks, 2016, [paper](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.75.6306&rep=rep1&type=pdf), [code](), [blog]()
- An End-to-End Trainable Neural Network for Image-based Sequence Recognition and Its Application to Scene Text Recognition, arXiv2015Jul, [paper](https://arxiv.org/abs/1507.05717), [code](), [blog]()


### 4.2 Detection
- PixelLink: Detecting Scene Text via Instance Segmentation, arXiv2018Jan, [paper](https://arxiv.org/abs/1801.01315), [code](), [blog]()
- Multi-Oriented Scene Text Detection via Corner Localization and Region Segmentation, CVPR2018, [paper](https://arxiv.org/abs/1802.08948), [code](), [blog]()
- Feature Enhancement Network: A Refined Scene Text Detector, AAAI2018, [paper](https://arxiv.org/abs/1711.04249), [code](), [blog]()
- Deep Residual Text Detection Network for Scene Text, arXiv2017Nov, [paper](https://arxiv.org/abs/1711.04147), [code](), [blog]()
- Feature Enhancement Network: A Refined Scene Text Detector, arXiv2017Nov, [paper](https://arxiv.org/abs/1711.04249), [code](), [blog]()
- WeText: Scene Text Detection underWeak Supervision, arXiv2017Oct, [paper](https://arxiv.org/abs/1710.04826), [code](), [blog]()
- Deep Scene Text Detection with Connected Component Proposals, arXiv2017Aug, [paper](https://arxiv.org/abs/1708.05133), [code](), [blog]()
- R2CNN: Rotational Region CNN for Orientation Robust Scene Text Detection, arXiv2017Jun [paper](https://arxiv.org/abs/1706.09579), [code](), [blog]()
- Detecting Oriented Text in Natural Images by Linking Segments, arXiv2017Apr, [paper](https://arxiv.org/abs/1703.06520), [code](), [blog]() 
- Deep Direct Regression for Multi-Oriented Scene Text Detection, arXiv2017Mar, [paper](https://arxiv.org/abs/1703.08289), [code](), [blog]()
- WordSup: Exploiting Word Annotations for Character based Text Detection, ICCV2017, [paper](https://arxiv.org/abs/1708.06720), [code](), [blog]()
- **EAST: An Efficient and Accurate Scene Text Detector**, CVPR2017, [paper](https://arxiv.org/abs/1704.03155), [code](https://github.com/argman/EAST), [blog]()
- DenseBox: Unifying Landmark Localization with End to End Object Detection, arXiv2015Sep, [paper](https://arxiv.org/abs/1509.04874), [code](), [blog]()

## 5. Classification and Detection
**Too much to list**
- Cross-Domain Weakly-Supervised Object Detection through Progressive Domain Adaptation, [paper](https://arxiv.org/abs/1803.11365), [code](), [blog]()
- Improving Object Localization with Fitness NMS and Bounded IoU Loss, CVPR2018, [paper](https://arxiv.org/abs/1711.00164), [code](), [blog]()
- Multi-scale Location-aware Kernel Representation for Object Detection, CVPR2018, [paper](https://arxiv.org/abs/1804.00428), [code](), [blog]()
- Dynamic Zoom-in Network for Fast Object Detection in Large Images, CVPR2018 [paper](https://arxiv.org/abs/1711.05187), [code](), [blog]()
- Focal Loss for Dense Object Detection, arXiv2017Aug, [paper](https://arxiv.org/abs/1708.02002), [code](), [blog]()
- Integrating Scene Text and Visual Appearance for Fine-Grained Image Classification, arXiv2017May, [paper](https://arxiv.org/abs/1704.04613), [code](), [blog]()
- HyperNet: Towards Accurate Region Proposal Generation and Joint Object Detection, arXiv2016Apr, [paper](https://arxiv.org/abs/1604.00600), [code](), [blog]()

## 6. Others
- Tell Me Where to Look: Guided Attention Inference Network, CVPR2018, [paper](https://arxiv.org/abs/1802.10171), [code](), [blog]()
- Fast and Accurate Single Image Super-Resolution via Information Distillation Network, CVPR2018, [paper](https://arxiv.org/abs/1803.09454), [code](), [blog]()
- Convergence Analysis of Two-layer Neural Networks with ReLU Activation, NIPS2017, [paper](https://arxiv.org/abs/1705.09886), [code](), [blog]()
- Fast Face-swap Using Convolutional Neural Networks, ICCV2017, [paper](https://arxiv.org/abs/1611.09577), [code](), [blog]()



